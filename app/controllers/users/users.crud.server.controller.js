'use strict';
/**
 * Created by Ermek on 03.11.2015.
 */
var mongoose = require('mongoose'),
    errorHandler = require('../errors.server.controller'),
    User = mongoose.model('User'),
    _ = require('lodash');

//create account manager
exports.create_account_manager = function(req, res){
    var new_user = new User(req.body);
    new_user.roles = 'moderator';
    new_user.displayName = new_user.firstName + ' ' + new_user.lastName;
    new_user.save(function(err){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.json(new_user);
        }
    });
};
//
exports.create_manager = function(req, res){
    var new_user = new User(req.body);
    new_user.roles = 'manager';
    new_user.displayName = new_user.firstName + ' ' + new_user.lastName;
    new_user.save(function(err){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.json(new_user);
        }
    });
};
