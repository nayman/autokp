'use strict';

var mongoose = require('mongoose'),
	multer = require('multer');
/**
 * Module dependencies.
 */
exports.index = function(req, res) {
	res.render('index', {
		user: req.user || null,
		request: req
	});
};
exports.upload = multer({
	dest: './public/uploads',
	rename: function () {
		return mongoose.Types.ObjectId();
	}
});