'use strict';
/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    errorHandler = require('./errors.server.controller'),
    fs = require('fs'),
    path = require('path'),
    _ = require('lodash');

/**
 * List of Measures
 */
exports.list = function(req, res){
    var filePath = path.join(__dirname, '../../private/measures.json');
    var readStream = fs.createReadStream(filePath);
    fs.readFile(filePath, 'utf8', function(err, measures){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.send(measures);
        }
    });
};