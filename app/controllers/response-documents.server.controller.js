'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	ResponseDocument = mongoose.model('ResponseDocument'),
	_ = require('lodash');

/**
 * Create a Response document
 */
exports.create = function(req, res) {
	var responseDocument = new ResponseDocument(req.body);
	responseDocument.user = req.user;

	responseDocument.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(responseDocument);
		}
	});
};

/**
 * Show the current Response document
 */
exports.read = function(req, res) {
	res.jsonp(req.responseDocument);
};

/**
 * Update a Response document
 */
exports.update = function(req, res) {
	var responseDocument = req.responseDocument ;

	responseDocument = _.extend(responseDocument , req.body);

	responseDocument.save(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(responseDocument);
		}
	});
};

/**
 * Delete an Response document
 */
exports.delete = function(req, res) {
	var responseDocument = req.responseDocument ;

	responseDocument.remove(function(err) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(responseDocument);
		}
	});
};

/**
 * List of Response documents
 */
exports.list = function(req, res) { 
	ResponseDocument.find().sort('-created').populate('user', 'displayName').exec(function(err, responseDocuments) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(responseDocuments);
		}
	});
};

/**
 * Response document middleware
 */
exports.responseDocumentByID = function(req, res, next, id) { 
	ResponseDocument.findById(id).populate('user', 'displayName').exec(function(err, responseDocument) {
		if (err) return next(err);
		if (! responseDocument) return next(new Error('Failed to load Response document ' + id));
		req.responseDocument = responseDocument ;
		next();
	});
};

/**
 * Response document authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
	if (req.responseDocument.user.id !== req.user.id) {
		return res.status(403).send('User is not authorized');
	}
	next();
};
