'use strict';
/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    errorHandler = require('./errors.server.controller'),
    Material = mongoose.model('Material'),
    _ = require('lodash');
/**
 * Create a Material
 */
exports.create = function(req, res){
    var material = new Material(req.body);
    material.save(function(err){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.json(material);
        }
    });
};
/**
 * Show the current Material
 */
exports.read = function(req, res){
    var material = req.material;
    material.update({
        $inc: {
            hits: 1
        }
    }, function(err){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.json(req.material);
        }
    });
};
/**
 * Delete an Material
 */
exports.delete = function(req, res){
    var material = req.material;
    material.remove(function(err){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.json(material);
        }
    });
};
/**
 * List of Materials
 */
exports.list = function(req, res){
    var search = req.query.search;
    if(search) {
        search = search.replace(/([.?*+^$[\]\\(){}|-])/g, '\\$1');
        var reg = new RegExp(search, 'i');
        Material.find({
            name: {
                $regex: reg
            }
        }).limit(10).sort('-hits').exec(function(err, materials){
            if(err){
                return res.status(400).send({
                    message: errorHandler.getErrorMessage(err)
                });
            } else {
                res.json(materials);
            }
        });
    } else {
        Material.find().limit(10).sort('-hits').exec(function(err, materials){
            if(err){
                return res.status(400).send({
                    message: errorHandler.getErrorMessage(err)
                });
            } else {
                res.json(materials);
            }
        });
    }

};
/**
 * Material middleware
 */
exports.materialByID = function(req, res, next, id){
    Material.findById(id).exec(function(err, material){
        if(err) return next(err);
        if(!material) return next(new Error('Failed to load Material ' + id));
        req.material = material;
        next();
    });
};

/**
 * Material authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
    if ('admin' !== req.user.role) {
        return res.status(403).send('User is not authorized');
    }
    next();
};