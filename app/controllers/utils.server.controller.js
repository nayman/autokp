/**
 * Created by Ermek on 26.10.2015.
 */

'use strict';

var _ = require('lodash');

/**
 * Extend utils's controller
 */
module.exports = _.extend(
    require('./utils/id.server.controller')
);
