// by bwin on 12/7/15.

'use strict';

/**
 * Module dependencies.
 */
var errorHandler = require('./errors.server.controller'),
    utils = require('./utils.server.controller'),
    nodemailer = require('nodemailer'),
    transporter = nodemailer.createTransport({
        service: 'Gmail',
        //host: 'mail.stml.kz',
        //port: 25,
        auth: {
            user: process.env.E || 'account.check.no.reply@gmail.com',
            pass: process.env.P || '1234wimf1234wimf'
        }
        //tls: {
        //    rejectUnauthorized: false
        //}
    }),
    _ = require('lodash');

/**
 * Create a article
 */
exports.send_mail = function(req, res) {
    var message = req.body;
    var mailOptions = {
        from: 'account.check.no.reply@gmail.com',
        to: 'yz5zt2ly@robot.zapier.com',
        subject: 'Новый заказ',
        html: message.name + ': ' + message.phone
    };
    transporter.sendMail(mailOptions, function(err){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.status(200).send();
        }
    });
};
