/**
 * Created by Ermek on 26.10.2015.
 */

'use strict';

exports.get_id = function(model){
    return (model._id || model).toString();
};
