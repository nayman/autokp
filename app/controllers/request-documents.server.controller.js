'use strict';
/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    errorHandler = require('./errors.server.controller'),
    utils = require('./utils.server.controller'),
    nodemailer = require('nodemailer'),
    async = require('async'),
    RequestDocument = mongoose.model('RequestDocument'),
    Provider = mongoose.model('Provider'),
    swig = require('swig'),
    Doc = mongoose.model('Doc'),
    Material = mongoose.model('Material'),
    transporter = nodemailer.createTransport({
        service: 'Mandrill',
        //host: 'mail.stml.kz',
        //port: 25,
        auth: {
            user: process.env.EMAIL_USER || 'ualixan88@gmail.com',
            pass: process.env.EMAIL_PWD || 'cdKrYPGxlFK4rDjfRftT9A'
        },
        //tls: {
        //    rejectUnauthorized: false
        //}
    }),
    _ = require('lodash');
function populate(model, callback){
    RequestDocument.findById(utils.get_id(model))
        .populate({path: 'docs'})
        .populate('user', 'displayName')
        .exec(function(find_err, found_req_doc){
            if(find_err){
                callback(find_err, null);
            } else {
                var ops = [{
                    path: 'docs.material',
                    model: 'Material'
                }];
                RequestDocument.populate(found_req_doc, ops, function(populate_err, populated_requestDocument){
                    if(populate_err){
                        callback(populate_err, null);
                    } else {
                        callback(null, populated_requestDocument);
                    }
                });
            }
        });
}
exports.accept_request = function(req, res){
    var requestDocument = req.requestDocument;
    if(requestDocument.locked){
        return res.status(400).send({
            message: 'Request Document locked'
        });
    }
    populate(requestDocument, function(err, db_requestDocument){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.json(db_requestDocument);
        }
    });
};
exports.send_client = function(req, res){
    var requestDocument = req.requestDocument;
    requestDocument.docs.forEach(function(doc){
        doc.responses_documents = doc.responses_documents.filter(function(item){
            return item.selected === true;
        });
        //doc.responses_documents.forEach(function(item){
        //    item.answer.price += doc.overload;
        //    item.answer.price_deliver += doc.overload;
        //});
    });
    swig.renderFile(__dirname + '../../templates/email/client-response.template.html', {
        requestDocument: requestDocument
    }, function (err, output) {
        if (err) {
            throw err;
        }
        var mailOptions = {
            from: 'send@stml.kz',
            to: requestDocument.client.email,
            subject: 'Срочно для '+requestDocument.client.name+'! Коммерческое предложение от Alasha Invest',
            html: output
        };
        transporter.sendMail(mailOptions, function(err){
            if(err){
                return res.status(400).send({
                    message: errorHandler.getErrorMessage(err)
                });
            } else {
                res.status(200).send();
            }
        });
    });
};
/*
 * Send email to provider
 * */
exports.send_email = function(req, res){
    var requestDocument = req.requestDocument;
    if(requestDocument.locked){
        return res.status(400).send({
            message: 'Request Document locked'
        });
    }
    var providers = req.body.selected_providers;
    var selected_docs = req.body.selected_docs;
    var parsed = JSON.stringify(selected_docs);
    var fns = [];
    providers.forEach(function(provider){
        var fn = function(callback){
            Provider.findById(provider, function(err, db_provider){
                if(err){
                    callback(err, null);
                } else {
                    var html4 = 'Уважаемый поставщик, просим вас ответить на наш запрос. Ссылка ниже <br>';
                    var html1 = '<a href="http://stml.kz:3000/#!/request-documents/accept/' + utils.get_id(requestDocument) + '?email=' + db_provider.email;
                    var html2 = '&d=' + escape(parsed);
                    var html3 = '">Ссылка</a>';
                    var mailOptions = {
                        from: 'send@stml.kz',
                        to: db_provider.email,
                        subject: 'Срочно - Запрос на цену от компании Alasha Invest',
                        text: 'http://stml.kz:3000' + '/#!/request-documents/accept/' + utils.get_id(requestDocument) + '',
                        html: html4+html1 + html2 + html3
                    };
                    transporter.sendMail(mailOptions, callback);
                }
            });
        };
        fns.push(fn);
    });
    async.series(fns, function(err, results){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.json(requestDocument);
        }
    });
};
/**
 * Create a Request document
 */
exports.create = function(req, res){
    var requestDocument = new RequestDocument(req.body);
    requestDocument.user = utils.get_id(req.user);
    requestDocument.save(function(err){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            RequestDocument.findById(requestDocument._id)
                .populate('provider', 'name email')
                .populate('docs', 'name')
                .populate('user', 'displayName')
                .exec(function(new_err, db_requestDocument){
                    if(new_err){
                        return res.status(400).send({
                            message: errorHandler.getErrorMessage(new_err)
                        });
                    } else {
                        res.jsonp(db_requestDocument);
                    }
                });
        }
    });
};
exports.responded_documents = function(req, res){
    RequestDocument.find({
        responded: true,
        locked: false
    }).sort('-updated')
        .exec(function(err, rdocs){
            if(err){
                return res.status(400).send({
                    message: errorHandler.getErrorMessage(err)
                });
            } else {
                res.json(rdocs);
            }
        });
};
exports.list_locked = function(req, res){
    RequestDocument.find({
        locked: true,
        responded: true
    }).sort('-created').exec(function(err, r_docs){
       if(err){
           return res.status(400).send({
               message: 'Request Document locked'
           });
       }  else {
           res.json(r_docs);
       }
    });
};
exports.answer_request = function(req, res){
    var requestDocument = req.requestDocument;
    var docs = req.body.docs;
    if(requestDocument.locked){
        return res.status(400).send({
            message: 'Request Document locked'
        });
    }
    var email = req.query.email;
    var fns = [];

    docs.forEach(function(_doc){
        (function(doc){
            fns.push(function(callback){
                if(doc.new_response){
                    doc.responses_documents = doc.responses_documents || [];
                    doc.responses_documents.push({
                        provider_email: email,
                        answer: doc.new_response
                    });
                    delete doc.material;
                    delete doc.new_response;
                    Doc.findById(utils.get_id(doc), function(f_err, f_doc){
                        if(f_err){
                            callback(f_err);
                        } else {
                            f_doc.responses_documents = doc.responses_documents;
                            f_doc.save(callback);
                        }
                    });
                    //Doc.update({
                    //    id: utils.get_id(doc)
                    //}, doc, callback);
                } else {
                    callback();
                }
                //Doc.update({
                //    id: utils.get_id(doc)
                //}, doc, callback);
            });
        })(_doc);
    });
    async.parallel(fns, function(err, result){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            requestDocument.updated = new Date();
            requestDocument.responded = true;
            requestDocument.save(function(s_err){
               if(s_err){
                   return res.status(400).send({
                       message: errorHandler.getErrorMessage(s_err)
                   });
               } else {
                   populate(requestDocument, function(err1, db_request_document){
                       if(err1){
                           return res.status(400).send({
                               message: errorHandler.getErrorMessage(err1)
                           });
                       } else {
                           res.json(db_request_document);
                       }
                   });
               }
            });
        }
    });
};
/**
 * Show the current Request document
 */
exports.read = function(req, res){
    res.jsonp(req.requestDocument);
};
exports.request_doc_response = function(req, res){
    var requestDocument = req.requestDocument;
    res.jsonp(requestDocument);
};
exports.add_file = function(req, res){
    var requestDocument = req.requestDocument;
    delete req.body.docs;
    requestDocument = _.extend(requestDocument, req.body);
    requestDocument.save(function(err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.status(200).send();
        }
    });
};
exports.update_request_document = function(req, res){
    var requestDocument = req.requestDocument;
    delete req.body.docs;
    requestDocument = _.extend(requestDocument, req.body);
    requestDocument.save(function(err) {
        if (err) {
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            populate(requestDocument, function(err1, db_request_document){
                if(err1){
                    return res.status(400).send({
                        message: errorHandler.getErrorMessage(err1)
                    });
                } else {
                    res.json(db_request_document);
                }
            });
        }
    });
};
/**
 * Update a Request document
 */
exports.update = function(req, res){
    var requestDocument = req.requestDocument;
    //if(requestDocument.locked && req.user.role !== 'manager'){
    //    return res.status(400).send({
    //        message: 'Request Document locked'
    //    });
    //}
    requestDocument = _.extend(requestDocument, req.body);
    var fns = [];

    requestDocument.docs.forEach(function(_doc){
        (function(doc){
            fns.push(function(callback){
                delete doc.material;
                Doc.findById(utils.get_id(doc), function(f_err, f_doc){
                    if(f_err){
                        callback(f_err);
                    } else {
                        f_doc.self_price = doc.self_price;
                        f_doc.self_price_deliver = doc.self_price_deliver;
                        f_doc.overload = doc.overload;
                        f_doc.responses_documents = doc.responses_documents;
                        f_doc.save(callback);
                    }
                });
            });
        })(_doc);
    });
    async.parallel(fns, function(err, result){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            requestDocument.responded = req.body.responded;
            requestDocument.save(function(sErr){
               if(sErr) {
                   return res.status(400).send({
                       message: errorHandler.getErrorMessage(sErr)
                   });
               } else {
                   populate(requestDocument, function(err1, db_request_document){
                       if(err1){
                           return res.status(400).send({
                               message: errorHandler.getErrorMessage(err1)
                           });
                       } else {
                           res.json(db_request_document);
                       }
                   });
               }
            });
        }
    });
};
/**
 * Delete an Request document
 */
exports.delete = function(req, res){
    var requestDocument = req.requestDocument;
    requestDocument.remove(function(err){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(requestDocument);
        }
    });
};
/**
 * delete doc from requestDoc
 */
exports.remove_doc = function(req, res){
    var requestDocument = req.requestDocument;
    //if(requestDocument.locked && req.user.role !== 'manager'){
    //    return res.status(400).send({
    //        message: 'Request Document locked'
    //    });
    //}
    var doc_id = req.query.docId;
    Doc.remove({
        _id: doc_id
    }, function(err){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            requestDocument.update({
                locked: false,
                $pull: {
                    docs: doc_id
                }
            }, function(update_err){
                if(update_err){
                    return res.status(400).send({
                        message: errorHandler.getErrorMessage(update_err)
                    });
                } else {
                    requestDocument.docs = requestDocument.docs.filter(function(_doc){
                        return doc_id !== utils.get_id(_doc);
                    });
                    res.jsonp(requestDocument);
                }
            });
        }
    });
};

exports.lock = function(req, res){
    var request_document = req.requestDocument;
    request_document.locked = true;
    request_document.save(function(err){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.json(request_document);
        }
    });
};
exports.unlock = function(req, res){
    var request_document = req.requestDocument;
    request_document.locked = false;
    request_document.save(function(err){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.json(request_document);
        }
    });
};
exports.add_doc = function(req, res){
    var request_document = req.requestDocument;
    //if(request_document.locked && req.user.role !== 'manager'){
    //    return res.status(400).send({
    //        message: 'Request Document locked'
    //    });
    //}
    async.waterfall([
        function create_new_material(callback){
            var new_material = req.body.new_material;
            if(new_material){
                var material = new Material(new_material);
                material.save(function(err){
                    if(err){
                        callback(err, null);
                    } else {
                        callback(null, material);
                    }
                });
            } else {
                callback(null, req.body.material);
            }
        },
        function create_new_doc(material, callback){
            var doc = new Doc(req.body.new_document);
            if(material){
                doc.material = utils.get_id(material);
            }
            Material.update({
                id: utils.get_id(doc.material)
            }, {
                $inc: {
                    hits: 1
                }
            }, function(update_err){
                if(update_err){
                    callback(update_err, null);
                } else {
                    doc.user = req.user;
                    doc.save(function(err){
                        if(err){
                            callback(err, null);
                        } else {
                            callback(null, doc);
                        }
                    });
                }
            });
        },
        function push_doc(doc, callback){
            request_document.locked = false;
            request_document.docs.push(doc);
            request_document.update({
                locked: false,
                $push: {
                    docs: doc
                }
            }, function(err){
                if(err)
                    return callback(err, null);
                else
                    callback(null, request_document);
            });
        }
    ], function(err, result){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            populate(result, function(populate_err, db_request_document){
                if(populate_err){
                    return res.status(400).send({
                        message: errorHandler.getErrorMessage(populate_err)
                    });
                } else {
                    res.json(db_request_document);
                }
            });
        }
    });
};
exports.read_by_excel = function(req, res){
    var request_document = req.requestDocument;
    res.json(request_document);
};
/**
 * List of Request documents
 */
exports.list = function(req, res){
    RequestDocument.find({
        locked: false
    }).sort('-created').populate('user', 'displayName').exec(function(err, requestDocuments){
        if(err){
            return res.status(400).send({
                message: errorHandler.getErrorMessage(err)
            });
        } else {
            res.jsonp(requestDocuments);
        }
    });
};
exports.requestDocumentByExcelID = function(req, res, next, id){
    RequestDocument.findOne({
        excel_id: id
    })
        .populate({path: 'docs'})
        .populate('user', 'displayName')
        .exec(function(err, requestDocument){
            if(err){
                return next(err);
            } else if(!requestDocument){
                var new_requestDocument = new RequestDocument({
                    excel_id: id,
                    user: utils.get_id(req.user)
                });
                new_requestDocument.save(function(save_err){
                    if(save_err){
                        return next(save_err);
                    } else {
                        req.requestDocument = new_requestDocument;
                        next();
                    }
                });
            } else {
                populate(requestDocument, function(populate_err, db_request_document){
                    if(populate_err){
                        return next(populate_err);
                    } else {
                        req.requestDocument = db_request_document;
                        next();
                    }
                });
            }
        });
};
/**
 * Request document middleware
 */
exports.requestDocumentByID = function(req, res, next, id){
    RequestDocument.findById(id)
        .populate('docs', 'name amount measure notes')
        .populate('user', 'displayName')
        .exec(function(err, requestDocument){
            if(err) return next(err);
            if(!requestDocument) return next(new Error('Failed to load Request document ' + id));
            populate(requestDocument, function(populate_err, db_request_document){
                if(populate_err){
                    return next(populate_err);
                } else {
                    req.requestDocument = db_request_document;
                    next();
                }
            });
        });
};
/**
 * Request document authorization middleware
 */
exports.hasAuthorization = function(req, res, next){
    if(req.requestDocument.user.id !== req.user.id){
        return res.status(403).send('User is not authorized');
    }
    next();
};
