'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Material Schema
 */
var MaterialSchema = new Schema({
	name: {
		type: String,
		default: '',
		required: 'Please fill Material name',
		trim: true
	},
	created: {
		type: Date,
		default: Date.now
	},
	hits: {
		type: Number,
		default: 0
	}
});

mongoose.model('Material', MaterialSchema);