'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Provider Schema
 */
var ProviderSchema = new Schema({
	name: {
		type: String,
		default: '',
		required: 'Please fill Provider name',
		trim: true
	},
	email: {
		type: String,
		default: '',
		required: 'Please fill Provider email',
		trim: true
	},
	created: {
		type: Date,
		default: Date.now
	},
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	}
});

mongoose.model('Provider', ProviderSchema);
