'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Request document Schema
 */
var RequestDocumentSchema = new Schema({
	client: {
		email: {
			type: String
		},
		city: {
			type: String
		},
		phone: {
			type: String
		},
		name: {
			type: String
		}
	},
	updated: {
		type: Date
	},
	responded: {
		type: Boolean,
		default: false
	},
	excel_id:{
		type: String
	},
	file_src: {
		type: String
	},
	docs: [{
		type: Schema.ObjectId,
		ref: 'Doc'
	}],
	locked: {
		type: Boolean,
		default: false
	},
	created: {
		type: Date,
		default: Date.now
	},
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	}
});

mongoose.model('RequestDocument', RequestDocumentSchema);
