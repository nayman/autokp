'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Doc Schema
 */
var DocSchema = new Schema({
	material: {
		type: Schema.ObjectId,
		ref: 'Material'
	},
	amount: {
		type: Number,
		default: 0
	},
	measure: {
		type: String,
		default: '',
		trim: true
	},
	notes: {
		type: String,
		default: '',
		required: 'Please fill Doc options',
		trim: true
	},
	responses_documents: [{
		provider_email: String,
		answer: {
			price: Number,
			price_deliver: Number,
			exist: Boolean,
			notes: String
		},
		self_registered: {
			type: Boolean,
			default: false
		},
		selected: {
			type: Boolean,
			default: false
		}
	}],
	self_price: {
		type: Number,
		default: 0
	},
	self_price_deliver: {
		type: Number,
		default: 0
	},
	overload: {
		type: Number,
		default: 0
	},
	created: {
		type: Date,
		default: Date.now
	},
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	}
});

mongoose.model('Doc', DocSchema);
