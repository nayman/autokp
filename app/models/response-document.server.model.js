'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Response document Schema
 */
var ResponseDocumentSchema = new Schema({
	provider: {
		type: String
	},
	response_documents: [{
		doc: {
			type: Schema.ObjectId,
			ref: 'Doc'
		},
		response: {
			price: Number,
			price_deliver: Number,
			exist: Boolean,
			notes: String
		},
		selected: {
			type: Boolean,
			default: false
		}
	}]
});

mongoose.model('ResponseDocument', ResponseDocumentSchema);