'use strict';

var should = require('should'),
	request = require('supertest'),
	app = require('../../server'),
	mongoose = require('mongoose'),
	User = mongoose.model('User'),
	RequestDocument = mongoose.model('RequestDocument'),
	agent = request.agent(app);

/**
 * Globals
 */
var credentials, user, requestDocument;

/**
 * Request document routes tests
 */
describe('Request document CRUD tests', function() {
	beforeEach(function(done) {
		// Create user credentials
		credentials = {
			username: 'username',
			password: 'password'
		};

		// Create a new user
		user = new User({
			firstName: 'Full',
			lastName: 'Name',
			displayName: 'Full Name',
			email: 'test@test.com',
			username: credentials.username,
			password: credentials.password,
			provider: 'local'
		});

		// Save a user to the test db and create new Request document
		user.save(function() {
			requestDocument = {
				name: 'Request document Name'
			};

			done();
		});
	});

	it('should be able to save Request document instance if logged in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Request document
				agent.post('/request-documents')
					.send(requestDocument)
					.expect(200)
					.end(function(requestDocumentSaveErr, requestDocumentSaveRes) {
						// Handle Request document save error
						if (requestDocumentSaveErr) done(requestDocumentSaveErr);

						// Get a list of Request documents
						agent.get('/request-documents')
							.end(function(requestDocumentsGetErr, requestDocumentsGetRes) {
								// Handle Request document save error
								if (requestDocumentsGetErr) done(requestDocumentsGetErr);

								// Get Request documents list
								var requestDocuments = requestDocumentsGetRes.body;

								// Set assertions
								(requestDocuments[0].user._id).should.equal(userId);
								(requestDocuments[0].name).should.match('Request document Name');

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should not be able to save Request document instance if not logged in', function(done) {
		agent.post('/request-documents')
			.send(requestDocument)
			.expect(401)
			.end(function(requestDocumentSaveErr, requestDocumentSaveRes) {
				// Call the assertion callback
				done(requestDocumentSaveErr);
			});
	});

	it('should not be able to save Request document instance if no name is provided', function(done) {
		// Invalidate name field
		requestDocument.name = '';

		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Request document
				agent.post('/request-documents')
					.send(requestDocument)
					.expect(400)
					.end(function(requestDocumentSaveErr, requestDocumentSaveRes) {
						// Set message assertion
						(requestDocumentSaveRes.body.message).should.match('Please fill Request document name');
						
						// Handle Request document save error
						done(requestDocumentSaveErr);
					});
			});
	});

	it('should be able to update Request document instance if signed in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Request document
				agent.post('/request-documents')
					.send(requestDocument)
					.expect(200)
					.end(function(requestDocumentSaveErr, requestDocumentSaveRes) {
						// Handle Request document save error
						if (requestDocumentSaveErr) done(requestDocumentSaveErr);

						// Update Request document name
						requestDocument.name = 'WHY YOU GOTTA BE SO MEAN?';

						// Update existing Request document
						agent.put('/request-documents/' + requestDocumentSaveRes.body._id)
							.send(requestDocument)
							.expect(200)
							.end(function(requestDocumentUpdateErr, requestDocumentUpdateRes) {
								// Handle Request document update error
								if (requestDocumentUpdateErr) done(requestDocumentUpdateErr);

								// Set assertions
								(requestDocumentUpdateRes.body._id).should.equal(requestDocumentSaveRes.body._id);
								(requestDocumentUpdateRes.body.name).should.match('WHY YOU GOTTA BE SO MEAN?');

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should be able to get a list of Request documents if not signed in', function(done) {
		// Create new Request document model instance
		var requestDocumentObj = new RequestDocument(requestDocument);

		// Save the Request document
		requestDocumentObj.save(function() {
			// Request Request documents
			request(app).get('/request-documents')
				.end(function(req, res) {
					// Set assertion
					res.body.should.be.an.Array.with.lengthOf(1);

					// Call the assertion callback
					done();
				});

		});
	});


	it('should be able to get a single Request document if not signed in', function(done) {
		// Create new Request document model instance
		var requestDocumentObj = new RequestDocument(requestDocument);

		// Save the Request document
		requestDocumentObj.save(function() {
			request(app).get('/request-documents/' + requestDocumentObj._id)
				.end(function(req, res) {
					// Set assertion
					res.body.should.be.an.Object.with.property('name', requestDocument.name);

					// Call the assertion callback
					done();
				});
		});
	});

	it('should be able to delete Request document instance if signed in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Request document
				agent.post('/request-documents')
					.send(requestDocument)
					.expect(200)
					.end(function(requestDocumentSaveErr, requestDocumentSaveRes) {
						// Handle Request document save error
						if (requestDocumentSaveErr) done(requestDocumentSaveErr);

						// Delete existing Request document
						agent.delete('/request-documents/' + requestDocumentSaveRes.body._id)
							.send(requestDocument)
							.expect(200)
							.end(function(requestDocumentDeleteErr, requestDocumentDeleteRes) {
								// Handle Request document error error
								if (requestDocumentDeleteErr) done(requestDocumentDeleteErr);

								// Set assertions
								(requestDocumentDeleteRes.body._id).should.equal(requestDocumentSaveRes.body._id);

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should not be able to delete Request document instance if not signed in', function(done) {
		// Set Request document user 
		requestDocument.user = user;

		// Create new Request document model instance
		var requestDocumentObj = new RequestDocument(requestDocument);

		// Save the Request document
		requestDocumentObj.save(function() {
			// Try deleting Request document
			request(app).delete('/request-documents/' + requestDocumentObj._id)
			.expect(401)
			.end(function(requestDocumentDeleteErr, requestDocumentDeleteRes) {
				// Set message assertion
				(requestDocumentDeleteRes.body.message).should.match('User is not logged in');

				// Handle Request document error error
				done(requestDocumentDeleteErr);
			});

		});
	});

	afterEach(function(done) {
		User.remove().exec();
		RequestDocument.remove().exec();
		done();
	});
});