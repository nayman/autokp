'use strict';

var should = require('should'),
	request = require('supertest'),
	app = require('../../server'),
	mongoose = require('mongoose'),
	User = mongoose.model('User'),
	Material = mongoose.model('Material'),
	agent = request.agent(app);

/**
 * Globals
 */
var credentials, user, material;

/**
 * Material routes tests
 */
describe('Material CRUD tests', function() {
	beforeEach(function(done) {
		// Create user credentials
		credentials = {
			username: 'username',
			password: 'password'
		};

		// Create a new user
		user = new User({
			firstName: 'Full',
			lastName: 'Name',
			displayName: 'Full Name',
			email: 'test@test.com',
			username: credentials.username,
			password: credentials.password,
			provider: 'local'
		});

		// Save a user to the test db and create new Material
		user.save(function() {
			material = {
				name: 'Material Name'
			};

			done();
		});
	});

	it('should be able to save Material instance if logged in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Material
				agent.post('/materials')
					.send(material)
					.expect(200)
					.end(function(materialSaveErr, materialSaveRes) {
						// Handle Material save error
						if (materialSaveErr) done(materialSaveErr);

						// Get a list of Materials
						agent.get('/materials')
							.end(function(materialsGetErr, materialsGetRes) {
								// Handle Material save error
								if (materialsGetErr) done(materialsGetErr);

								// Get Materials list
								var materials = materialsGetRes.body;

								// Set assertions
								(materials[0].user._id).should.equal(userId);
								(materials[0].name).should.match('Material Name');

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should not be able to save Material instance if not logged in', function(done) {
		agent.post('/materials')
			.send(material)
			.expect(401)
			.end(function(materialSaveErr, materialSaveRes) {
				// Call the assertion callback
				done(materialSaveErr);
			});
	});

	it('should not be able to save Material instance if no name is provided', function(done) {
		// Invalidate name field
		material.name = '';

		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Material
				agent.post('/materials')
					.send(material)
					.expect(400)
					.end(function(materialSaveErr, materialSaveRes) {
						// Set message assertion
						(materialSaveRes.body.message).should.match('Please fill Material name');
						
						// Handle Material save error
						done(materialSaveErr);
					});
			});
	});

	it('should be able to update Material instance if signed in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Material
				agent.post('/materials')
					.send(material)
					.expect(200)
					.end(function(materialSaveErr, materialSaveRes) {
						// Handle Material save error
						if (materialSaveErr) done(materialSaveErr);

						// Update Material name
						material.name = 'WHY YOU GOTTA BE SO MEAN?';

						// Update existing Material
						agent.put('/materials/' + materialSaveRes.body._id)
							.send(material)
							.expect(200)
							.end(function(materialUpdateErr, materialUpdateRes) {
								// Handle Material update error
								if (materialUpdateErr) done(materialUpdateErr);

								// Set assertions
								(materialUpdateRes.body._id).should.equal(materialSaveRes.body._id);
								(materialUpdateRes.body.name).should.match('WHY YOU GOTTA BE SO MEAN?');

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should be able to get a list of Materials if not signed in', function(done) {
		// Create new Material model instance
		var materialObj = new Material(material);

		// Save the Material
		materialObj.save(function() {
			// Request Materials
			request(app).get('/materials')
				.end(function(req, res) {
					// Set assertion
					res.body.should.be.an.Array.with.lengthOf(1);

					// Call the assertion callback
					done();
				});

		});
	});


	it('should be able to get a single Material if not signed in', function(done) {
		// Create new Material model instance
		var materialObj = new Material(material);

		// Save the Material
		materialObj.save(function() {
			request(app).get('/materials/' + materialObj._id)
				.end(function(req, res) {
					// Set assertion
					res.body.should.be.an.Object.with.property('name', material.name);

					// Call the assertion callback
					done();
				});
		});
	});

	it('should be able to delete Material instance if signed in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Material
				agent.post('/materials')
					.send(material)
					.expect(200)
					.end(function(materialSaveErr, materialSaveRes) {
						// Handle Material save error
						if (materialSaveErr) done(materialSaveErr);

						// Delete existing Material
						agent.delete('/materials/' + materialSaveRes.body._id)
							.send(material)
							.expect(200)
							.end(function(materialDeleteErr, materialDeleteRes) {
								// Handle Material error error
								if (materialDeleteErr) done(materialDeleteErr);

								// Set assertions
								(materialDeleteRes.body._id).should.equal(materialSaveRes.body._id);

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should not be able to delete Material instance if not signed in', function(done) {
		// Set Material user 
		material.user = user;

		// Create new Material model instance
		var materialObj = new Material(material);

		// Save the Material
		materialObj.save(function() {
			// Try deleting Material
			request(app).delete('/materials/' + materialObj._id)
			.expect(401)
			.end(function(materialDeleteErr, materialDeleteRes) {
				// Set message assertion
				(materialDeleteRes.body.message).should.match('User is not logged in');

				// Handle Material error error
				done(materialDeleteErr);
			});

		});
	});

	afterEach(function(done) {
		User.remove().exec();
		Material.remove().exec();
		done();
	});
});