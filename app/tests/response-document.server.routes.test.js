'use strict';

var should = require('should'),
	request = require('supertest'),
	app = require('../../server'),
	mongoose = require('mongoose'),
	User = mongoose.model('User'),
	ResponseDocument = mongoose.model('ResponseDocument'),
	agent = request.agent(app);

/**
 * Globals
 */
var credentials, user, responseDocument;

/**
 * Response document routes tests
 */
describe('Response document CRUD tests', function() {
	beforeEach(function(done) {
		// Create user credentials
		credentials = {
			username: 'username',
			password: 'password'
		};

		// Create a new user
		user = new User({
			firstName: 'Full',
			lastName: 'Name',
			displayName: 'Full Name',
			email: 'test@test.com',
			username: credentials.username,
			password: credentials.password,
			provider: 'local'
		});

		// Save a user to the test db and create new Response document
		user.save(function() {
			responseDocument = {
				name: 'Response document Name'
			};

			done();
		});
	});

	it('should be able to save Response document instance if logged in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Response document
				agent.post('/response-documents')
					.send(responseDocument)
					.expect(200)
					.end(function(responseDocumentSaveErr, responseDocumentSaveRes) {
						// Handle Response document save error
						if (responseDocumentSaveErr) done(responseDocumentSaveErr);

						// Get a list of Response documents
						agent.get('/response-documents')
							.end(function(responseDocumentsGetErr, responseDocumentsGetRes) {
								// Handle Response document save error
								if (responseDocumentsGetErr) done(responseDocumentsGetErr);

								// Get Response documents list
								var responseDocuments = responseDocumentsGetRes.body;

								// Set assertions
								(responseDocuments[0].user._id).should.equal(userId);
								(responseDocuments[0].name).should.match('Response document Name');

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should not be able to save Response document instance if not logged in', function(done) {
		agent.post('/response-documents')
			.send(responseDocument)
			.expect(401)
			.end(function(responseDocumentSaveErr, responseDocumentSaveRes) {
				// Call the assertion callback
				done(responseDocumentSaveErr);
			});
	});

	it('should not be able to save Response document instance if no name is provided', function(done) {
		// Invalidate name field
		responseDocument.name = '';

		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Response document
				agent.post('/response-documents')
					.send(responseDocument)
					.expect(400)
					.end(function(responseDocumentSaveErr, responseDocumentSaveRes) {
						// Set message assertion
						(responseDocumentSaveRes.body.message).should.match('Please fill Response document name');
						
						// Handle Response document save error
						done(responseDocumentSaveErr);
					});
			});
	});

	it('should be able to update Response document instance if signed in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Response document
				agent.post('/response-documents')
					.send(responseDocument)
					.expect(200)
					.end(function(responseDocumentSaveErr, responseDocumentSaveRes) {
						// Handle Response document save error
						if (responseDocumentSaveErr) done(responseDocumentSaveErr);

						// Update Response document name
						responseDocument.name = 'WHY YOU GOTTA BE SO MEAN?';

						// Update existing Response document
						agent.put('/response-documents/' + responseDocumentSaveRes.body._id)
							.send(responseDocument)
							.expect(200)
							.end(function(responseDocumentUpdateErr, responseDocumentUpdateRes) {
								// Handle Response document update error
								if (responseDocumentUpdateErr) done(responseDocumentUpdateErr);

								// Set assertions
								(responseDocumentUpdateRes.body._id).should.equal(responseDocumentSaveRes.body._id);
								(responseDocumentUpdateRes.body.name).should.match('WHY YOU GOTTA BE SO MEAN?');

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should be able to get a list of Response documents if not signed in', function(done) {
		// Create new Response document model instance
		var responseDocumentObj = new ResponseDocument(responseDocument);

		// Save the Response document
		responseDocumentObj.save(function() {
			// Request Response documents
			request(app).get('/response-documents')
				.end(function(req, res) {
					// Set assertion
					res.body.should.be.an.Array.with.lengthOf(1);

					// Call the assertion callback
					done();
				});

		});
	});


	it('should be able to get a single Response document if not signed in', function(done) {
		// Create new Response document model instance
		var responseDocumentObj = new ResponseDocument(responseDocument);

		// Save the Response document
		responseDocumentObj.save(function() {
			request(app).get('/response-documents/' + responseDocumentObj._id)
				.end(function(req, res) {
					// Set assertion
					res.body.should.be.an.Object.with.property('name', responseDocument.name);

					// Call the assertion callback
					done();
				});
		});
	});

	it('should be able to delete Response document instance if signed in', function(done) {
		agent.post('/auth/signin')
			.send(credentials)
			.expect(200)
			.end(function(signinErr, signinRes) {
				// Handle signin error
				if (signinErr) done(signinErr);

				// Get the userId
				var userId = user.id;

				// Save a new Response document
				agent.post('/response-documents')
					.send(responseDocument)
					.expect(200)
					.end(function(responseDocumentSaveErr, responseDocumentSaveRes) {
						// Handle Response document save error
						if (responseDocumentSaveErr) done(responseDocumentSaveErr);

						// Delete existing Response document
						agent.delete('/response-documents/' + responseDocumentSaveRes.body._id)
							.send(responseDocument)
							.expect(200)
							.end(function(responseDocumentDeleteErr, responseDocumentDeleteRes) {
								// Handle Response document error error
								if (responseDocumentDeleteErr) done(responseDocumentDeleteErr);

								// Set assertions
								(responseDocumentDeleteRes.body._id).should.equal(responseDocumentSaveRes.body._id);

								// Call the assertion callback
								done();
							});
					});
			});
	});

	it('should not be able to delete Response document instance if not signed in', function(done) {
		// Set Response document user 
		responseDocument.user = user;

		// Create new Response document model instance
		var responseDocumentObj = new ResponseDocument(responseDocument);

		// Save the Response document
		responseDocumentObj.save(function() {
			// Try deleting Response document
			request(app).delete('/response-documents/' + responseDocumentObj._id)
			.expect(401)
			.end(function(responseDocumentDeleteErr, responseDocumentDeleteRes) {
				// Set message assertion
				(responseDocumentDeleteRes.body.message).should.match('User is not logged in');

				// Handle Response document error error
				done(responseDocumentDeleteErr);
			});

		});
	});

	afterEach(function(done) {
		User.remove().exec();
		ResponseDocument.remove().exec();
		done();
	});
});