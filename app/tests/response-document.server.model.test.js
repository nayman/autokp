'use strict';

/**
 * Module dependencies.
 */
var should = require('should'),
	mongoose = require('mongoose'),
	User = mongoose.model('User'),
	ResponseDocument = mongoose.model('ResponseDocument');

/**
 * Globals
 */
var user, responseDocument;

/**
 * Unit tests
 */
describe('Response document Model Unit Tests:', function() {
	beforeEach(function(done) {
		user = new User({
			firstName: 'Full',
			lastName: 'Name',
			displayName: 'Full Name',
			email: 'test@test.com',
			username: 'username',
			password: 'password'
		});

		user.save(function() { 
			responseDocument = new ResponseDocument({
				name: 'Response document Name',
				user: user
			});

			done();
		});
	});

	describe('Method Save', function() {
		it('should be able to save without problems', function(done) {
			return responseDocument.save(function(err) {
				should.not.exist(err);
				done();
			});
		});

		it('should be able to show an error when try to save without name', function(done) { 
			responseDocument.name = '';

			return responseDocument.save(function(err) {
				should.exist(err);
				done();
			});
		});
	});

	afterEach(function(done) { 
		ResponseDocument.remove().exec();
		User.remove().exec();

		done();
	});
});