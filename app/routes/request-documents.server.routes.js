'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var requestDocuments = require('../../app/controllers/request-documents.server.controller');

	// Request documents Routes
	app.route('/request-documents')
		.get(requestDocuments.list)
		.post(users.requiresLogin, requestDocuments.create);

	app.route('/request-documents/:requestDocumentId')
		.get(requestDocuments.read)
		.put(users.requiresLogin, requestDocuments.update)
		.delete(users.requiresLogin, requestDocuments.hasAuthorization, requestDocuments.delete);
	app.route('/request-documents-misc/:requestDocumentId/update_request_document').put(users.requiresLogin, requestDocuments.update_request_document);
	app.route('/request-documents-misc/:requestDocumentId/add_file').put(requestDocuments.add_file);


	app.route('/request-documents_excel/:excelId').get(requestDocuments.read_by_excel);
	app.route('/request-documents/:requestDocumentId/lock').post(requestDocuments.lock);
	app.route('/request-documents/:requestDocumentId/unlock').post(requestDocuments.unlock);
	app.route('/request-documents/:requestDocumentId/doc').post(requestDocuments.add_doc);
	app.route('/request-documents/:requestDocumentId/doc').delete(requestDocuments.remove_doc);
	app.route('/request-documents/:requestDocumentId/send_email').post(requestDocuments.send_email);
	app.route('/request-documents/:requestDocumentId/send_client').post(requestDocuments.send_client);
	app.route('/request-documents/accept/:requestDocumentId').get(requestDocuments.accept_request);
	app.route('/request-documents/accept/:requestDocumentId/answer').post(requestDocuments.answer_request);
	app.route('/request-documents_locked').get(requestDocuments.list_locked);


	app.route('/responded-documents').get(requestDocuments.responded_documents);
	// Finish by binding the Request document middleware
	app.param('requestDocumentId', requestDocuments.requestDocumentByID);
	app.param('excelId', requestDocuments.requestDocumentByExcelID);
};
