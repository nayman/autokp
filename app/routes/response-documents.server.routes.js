'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var responseDocuments = require('../../app/controllers/response-documents.server.controller');

	// Response documents Routes
	app.route('/response-documents')
		.get(responseDocuments.list)
		.post(users.requiresLogin, responseDocuments.create);

	app.route('/response-documents/:responseDocumentId')
		.get(responseDocuments.read)
		.put(users.requiresLogin, responseDocuments.hasAuthorization, responseDocuments.update)
		.delete(users.requiresLogin, responseDocuments.hasAuthorization, responseDocuments.delete);

	// Finish by binding the Response document middleware
	app.param('responseDocumentId', responseDocuments.responseDocumentByID);
};
