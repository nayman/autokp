// by bwin on 12/7/15.

'use strict';

/**
 * Module dependencies.
 */
var externals = require('../../app/controllers/external.server.controller');

module.exports = function(app) {
    // Article Routes
    app.route('/send_mail')
        .post(externals.send_mail);
};