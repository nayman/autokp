'use strict';


module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var materials = require('../../app/controllers/materials.server.controller');

	// Materials Routes
	app.route('/materials')
		.get(materials.list)
		.post(users.requiresLogin, materials.create);

	app.route('/materials/:materialId')
		.get(materials.read)
		.delete(users.requiresLogin, materials.delete);

	// Finish by binding the Material middleware
	app.param('materialId', materials.materialByID);
};
