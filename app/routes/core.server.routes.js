'use strict';

module.exports = function(app) {
	// Root routing
	var core = require('../../app/controllers/core.server.controller');
	app.route('/').get(core.index);
	app.post('/upload', core.upload, function(req, res){
		res.send(req.files.file.name);
	});
};
