// by bwin on 11/18/15.

'use strict';

module.exports = function(app) {
    var measures = require('../../app/controllers/measures.server.controller');

    // Materials Routes
    app.route('/measures')
        .get(measures.list);
};
