'use strict';


angular.module('core').controller('HomeController', ['$scope', 'Authentication', 'Providers', 'Docs', 'RequestDocuments',
	function($scope, Authentication, Providers, Docs, RequestDocuments) {
		// This provides Authentication context.
		$scope.authentication = Authentication;
		$scope.doc = {};
		// Create new Doc
		$scope.create_doc = function() {
			// Create new Doc object
			var doc = new Docs ({
				name: this.doc.name,
				measure: this.doc.measure,
				amount: this.doc.amount,
				notes: this.doc.notes
			});

			// Redirect after save
			doc.$save(function(response) {
				//$location.path('docs/' + response._id);
				// Clear form fields
				$scope.doc.name = '';
				$scope.doc.measure = '';
				$scope.doc.amount = 0;
				$scope.doc.notes = '';
				$scope.find_docs();
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};
		$scope.find_docs = function() {
			$scope.docs = Docs.query();
		};
		$scope.find_providers = function() {
			$scope.providers = Providers.query();
		};
		$scope.requestDocuments = [];
		$scope.resolve = function(){
			var requestDocument = new RequestDocuments({
				provider: $scope.provider_item._id || $scope.provider_item,
				docs: $scope.doc_item
			});
			requestDocument.$save(function(successResponse){
				$scope.provider_item = null;
				$scope.doc_item = null;
				$scope.requestDocuments.push(successResponse);
			});
		};
		$scope.new_provider = {};
		$scope.new_provider_create = function(){
			var provider = new Providers({
				name: $scope.new_provider.name,
				email: $scope.new_provider.email
			});
			provider.$save(function(successResponse){
				$scope.new_provider.name =  null;
				$scope.new_provider.email = null;
				var requestDocument = new RequestDocuments({
					provider: successResponse._id || successResponse,
					docs: $scope.doc_item
				});
				requestDocument.$save(function(successResponse){
					$scope.provider_item = null;
					$scope.doc_item = [];
					$scope.requestDocuments.push(successResponse);
					$scope.find_providers();
				});
			}, function(errorResponse){
				console.log(errorResponse);
			});
		};
		$scope.get_price = function(){
			angular.forEach($scope.requestDocuments, function(request_doc){
				RequestDocuments.send_email({
					requestDocumentId: (request_doc._id || request_doc)
				},{
					provider: request_doc.provider._id || request_doc.provider,
					docs: request_doc.docs
				});
			});
		};
	}
]);
