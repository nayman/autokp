'use strict';

// Materials controller
angular.module('materials').controller('MaterialsController', ['$scope', '$stateParams', '$location', 'Authentication', 'Materials',
	function($scope, $stateParams, $location, Authentication, Materials) {
		$scope.authentication = Authentication;

		// Create new Material
		$scope.create = function() {
			// Create new Material object
			var material = new Materials ({
				name: this.name
			});

			// Redirect after save
			material.$save(function(response) {
				$location.path('materials/' + response._id);

				// Clear form fields
				$scope.name = '';
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Remove existing Material
		$scope.remove = function(material) {
			if ( material ) { 
				material.$remove();

				for (var i in $scope.materials) {
					if ($scope.materials [i] === material) {
						$scope.materials.splice(i, 1);
					}
				}
			} else {
				$scope.material.$remove(function() {
					$location.path('materials');
				});
			}
		};

		// Update existing Material
		$scope.update = function() {
			var material = $scope.material;

			material.$update(function() {
				$location.path('materials/' + material._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Materials
		$scope.find = function() {
			$scope.materials = Materials.query();
		};

		// Find existing Material
		$scope.findOne = function() {
			$scope.material = Materials.get({ 
				materialId: $stateParams.materialId
			});
		};
	}
]);