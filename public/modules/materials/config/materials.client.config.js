'use strict';

// Configuring the Articles module
angular.module('materials').run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem('topbar', 'Материалы', 'materials', 'dropdown', '/materials(/create)?', true, ['manager'], 0);
		Menus.addSubMenuItem('topbar', 'materials', 'Список материалов', 'materials');
		Menus.addSubMenuItem('topbar', 'materials', 'Создать материал', 'materials/create');
	}
]);
