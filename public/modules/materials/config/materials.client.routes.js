'use strict';

//Setting up route
angular.module('materials').config(['$stateProvider',
	function($stateProvider) {
		// Materials state routing
		$stateProvider.
		state('listMaterials', {
			url: '/materials',
			templateUrl: 'modules/materials/views/list-materials.client.view.html'
		}).
		state('createMaterial', {
			url: '/materials/create',
			templateUrl: 'modules/materials/views/create-material.client.view.html'
		}).
		state('viewMaterial', {
			url: '/materials/:materialId',
			templateUrl: 'modules/materials/views/view-material.client.view.html'
		}).
		state('editMaterial', {
			url: '/materials/:materialId/edit',
			templateUrl: 'modules/materials/views/edit-material.client.view.html'
		});
	}
]);