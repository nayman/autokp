'use strict';

//Materials service used to communicate Materials REST endpoints
angular.module('materials').factory('Materials', ['$resource',
	function($resource) {
		return $resource('materials/:materialId', { materialId: '@_id'
		}, {
			update: {
				method: 'PUT'
			},
			measures: {
				method: 'GET',
				isArray: true,
				url: 'measures'
			}
		});
	}
]);