'use strict';

(function() {
	// Materials Controller Spec
	describe('Materials Controller Tests', function() {
		// Initialize global variables
		var MaterialsController,
		scope,
		$httpBackend,
		$stateParams,
		$location;

		// The $resource service augments the response object with methods for updating and deleting the resource.
		// If we were to use the standard toEqual matcher, our tests would fail because the test values would not match
		// the responses exactly. To solve the problem, we define a new toEqualData Jasmine matcher.
		// When the toEqualData matcher compares two objects, it takes only object properties into
		// account and ignores methods.
		beforeEach(function() {
			jasmine.addMatchers({
				toEqualData: function(util, customEqualityTesters) {
					return {
						compare: function(actual, expected) {
							return {
								pass: angular.equals(actual, expected)
							};
						}
					};
				}
			});
		});

		// Then we can start by loading the main application module
		beforeEach(module(ApplicationConfiguration.applicationModuleName));

		// The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
		// This allows us to inject a service but then attach it to a variable
		// with the same name as the service.
		beforeEach(inject(function($controller, $rootScope, _$location_, _$stateParams_, _$httpBackend_) {
			// Set a new global scope
			scope = $rootScope.$new();

			// Point global variables to injected services
			$stateParams = _$stateParams_;
			$httpBackend = _$httpBackend_;
			$location = _$location_;

			// Initialize the Materials controller.
			MaterialsController = $controller('MaterialsController', {
				$scope: scope
			});
		}));

		it('$scope.find() should create an array with at least one Material object fetched from XHR', inject(function(Materials) {
			// Create sample Material using the Materials service
			var sampleMaterial = new Materials({
				name: 'New Material'
			});

			// Create a sample Materials array that includes the new Material
			var sampleMaterials = [sampleMaterial];

			// Set GET response
			$httpBackend.expectGET('materials').respond(sampleMaterials);

			// Run controller functionality
			scope.find();
			$httpBackend.flush();

			// Test scope value
			expect(scope.materials).toEqualData(sampleMaterials);
		}));

		it('$scope.findOne() should create an array with one Material object fetched from XHR using a materialId URL parameter', inject(function(Materials) {
			// Define a sample Material object
			var sampleMaterial = new Materials({
				name: 'New Material'
			});

			// Set the URL parameter
			$stateParams.materialId = '525a8422f6d0f87f0e407a33';

			// Set GET response
			$httpBackend.expectGET(/materials\/([0-9a-fA-F]{24})$/).respond(sampleMaterial);

			// Run controller functionality
			scope.findOne();
			$httpBackend.flush();

			// Test scope value
			expect(scope.material).toEqualData(sampleMaterial);
		}));

		it('$scope.create() with valid form data should send a POST request with the form input values and then locate to new object URL', inject(function(Materials) {
			// Create a sample Material object
			var sampleMaterialPostData = new Materials({
				name: 'New Material'
			});

			// Create a sample Material response
			var sampleMaterialResponse = new Materials({
				_id: '525cf20451979dea2c000001',
				name: 'New Material'
			});

			// Fixture mock form input values
			scope.name = 'New Material';

			// Set POST response
			$httpBackend.expectPOST('materials', sampleMaterialPostData).respond(sampleMaterialResponse);

			// Run controller functionality
			scope.create();
			$httpBackend.flush();

			// Test form inputs are reset
			expect(scope.name).toEqual('');

			// Test URL redirection after the Material was created
			expect($location.path()).toBe('/materials/' + sampleMaterialResponse._id);
		}));

		it('$scope.update() should update a valid Material', inject(function(Materials) {
			// Define a sample Material put data
			var sampleMaterialPutData = new Materials({
				_id: '525cf20451979dea2c000001',
				name: 'New Material'
			});

			// Mock Material in scope
			scope.material = sampleMaterialPutData;

			// Set PUT response
			$httpBackend.expectPUT(/materials\/([0-9a-fA-F]{24})$/).respond();

			// Run controller functionality
			scope.update();
			$httpBackend.flush();

			// Test URL location to new object
			expect($location.path()).toBe('/materials/' + sampleMaterialPutData._id);
		}));

		it('$scope.remove() should send a DELETE request with a valid materialId and remove the Material from the scope', inject(function(Materials) {
			// Create new Material object
			var sampleMaterial = new Materials({
				_id: '525a8422f6d0f87f0e407a33'
			});

			// Create new Materials array and include the Material
			scope.materials = [sampleMaterial];

			// Set expected DELETE response
			$httpBackend.expectDELETE(/materials\/([0-9a-fA-F]{24})$/).respond(204);

			// Run controller functionality
			scope.remove(sampleMaterial);
			$httpBackend.flush();

			// Test array after successful delete
			expect(scope.materials.length).toBe(0);
		}));
	});
}());