'use strict';

//Response documents service used to communicate Response documents REST endpoints
angular.module('response-documents').factory('ResponseDocuments', ['$resource',
	function($resource) {
		return $resource('response-documents/:responseDocumentId', { responseDocumentId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);