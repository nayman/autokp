'use strict';

(function() {
	// Response documents Controller Spec
	describe('Response documents Controller Tests', function() {
		// Initialize global variables
		var ResponseDocumentsController,
		scope,
		$httpBackend,
		$stateParams,
		$location;

		// The $resource service augments the response object with methods for updating and deleting the resource.
		// If we were to use the standard toEqual matcher, our tests would fail because the test values would not match
		// the responses exactly. To solve the problem, we define a new toEqualData Jasmine matcher.
		// When the toEqualData matcher compares two objects, it takes only object properties into
		// account and ignores methods.
		beforeEach(function() {
			jasmine.addMatchers({
				toEqualData: function(util, customEqualityTesters) {
					return {
						compare: function(actual, expected) {
							return {
								pass: angular.equals(actual, expected)
							};
						}
					};
				}
			});
		});

		// Then we can start by loading the main application module
		beforeEach(module(ApplicationConfiguration.applicationModuleName));

		// The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
		// This allows us to inject a service but then attach it to a variable
		// with the same name as the service.
		beforeEach(inject(function($controller, $rootScope, _$location_, _$stateParams_, _$httpBackend_) {
			// Set a new global scope
			scope = $rootScope.$new();

			// Point global variables to injected services
			$stateParams = _$stateParams_;
			$httpBackend = _$httpBackend_;
			$location = _$location_;

			// Initialize the Response documents controller.
			ResponseDocumentsController = $controller('ResponseDocumentsController', {
				$scope: scope
			});
		}));

		it('$scope.find() should create an array with at least one Response document object fetched from XHR', inject(function(ResponseDocuments) {
			// Create sample Response document using the Response documents service
			var sampleResponseDocument = new ResponseDocuments({
				name: 'New Response document'
			});

			// Create a sample Response documents array that includes the new Response document
			var sampleResponseDocuments = [sampleResponseDocument];

			// Set GET response
			$httpBackend.expectGET('response-documents').respond(sampleResponseDocuments);

			// Run controller functionality
			scope.find();
			$httpBackend.flush();

			// Test scope value
			expect(scope.responseDocuments).toEqualData(sampleResponseDocuments);
		}));

		it('$scope.findOne() should create an array with one Response document object fetched from XHR using a responseDocumentId URL parameter', inject(function(ResponseDocuments) {
			// Define a sample Response document object
			var sampleResponseDocument = new ResponseDocuments({
				name: 'New Response document'
			});

			// Set the URL parameter
			$stateParams.responseDocumentId = '525a8422f6d0f87f0e407a33';

			// Set GET response
			$httpBackend.expectGET(/response-documents\/([0-9a-fA-F]{24})$/).respond(sampleResponseDocument);

			// Run controller functionality
			scope.findOne();
			$httpBackend.flush();

			// Test scope value
			expect(scope.responseDocument).toEqualData(sampleResponseDocument);
		}));

		it('$scope.create() with valid form data should send a POST request with the form input values and then locate to new object URL', inject(function(ResponseDocuments) {
			// Create a sample Response document object
			var sampleResponseDocumentPostData = new ResponseDocuments({
				name: 'New Response document'
			});

			// Create a sample Response document response
			var sampleResponseDocumentResponse = new ResponseDocuments({
				_id: '525cf20451979dea2c000001',
				name: 'New Response document'
			});

			// Fixture mock form input values
			scope.name = 'New Response document';

			// Set POST response
			$httpBackend.expectPOST('response-documents', sampleResponseDocumentPostData).respond(sampleResponseDocumentResponse);

			// Run controller functionality
			scope.create();
			$httpBackend.flush();

			// Test form inputs are reset
			expect(scope.name).toEqual('');

			// Test URL redirection after the Response document was created
			expect($location.path()).toBe('/response-documents/' + sampleResponseDocumentResponse._id);
		}));

		it('$scope.update() should update a valid Response document', inject(function(ResponseDocuments) {
			// Define a sample Response document put data
			var sampleResponseDocumentPutData = new ResponseDocuments({
				_id: '525cf20451979dea2c000001',
				name: 'New Response document'
			});

			// Mock Response document in scope
			scope.responseDocument = sampleResponseDocumentPutData;

			// Set PUT response
			$httpBackend.expectPUT(/response-documents\/([0-9a-fA-F]{24})$/).respond();

			// Run controller functionality
			scope.update();
			$httpBackend.flush();

			// Test URL location to new object
			expect($location.path()).toBe('/response-documents/' + sampleResponseDocumentPutData._id);
		}));

		it('$scope.remove() should send a DELETE request with a valid responseDocumentId and remove the Response document from the scope', inject(function(ResponseDocuments) {
			// Create new Response document object
			var sampleResponseDocument = new ResponseDocuments({
				_id: '525a8422f6d0f87f0e407a33'
			});

			// Create new Response documents array and include the Response document
			scope.responseDocuments = [sampleResponseDocument];

			// Set expected DELETE response
			$httpBackend.expectDELETE(/response-documents\/([0-9a-fA-F]{24})$/).respond(204);

			// Run controller functionality
			scope.remove(sampleResponseDocument);
			$httpBackend.flush();

			// Test array after successful delete
			expect(scope.responseDocuments.length).toBe(0);
		}));
	});
}());