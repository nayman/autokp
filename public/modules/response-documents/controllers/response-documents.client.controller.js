'use strict';

// Response documents controller
angular.module('response-documents').controller('ResponseDocumentsController', ['$scope', '$stateParams', '$location', 'Authentication', 'ResponseDocuments',
	function($scope, $stateParams, $location, Authentication, ResponseDocuments) {
		$scope.authentication = Authentication;

		// Create new Response document
		$scope.create = function() {
			// Create new Response document object
			var responseDocument = new ResponseDocuments ({
				name: this.name
			});

			// Redirect after save
			responseDocument.$save(function(response) {
				$location.path('response-documents/' + response._id);

				// Clear form fields
				$scope.name = '';
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Remove existing Response document
		$scope.remove = function(responseDocument) {
			if ( responseDocument ) { 
				responseDocument.$remove();

				for (var i in $scope.responseDocuments) {
					if ($scope.responseDocuments [i] === responseDocument) {
						$scope.responseDocuments.splice(i, 1);
					}
				}
			} else {
				$scope.responseDocument.$remove(function() {
					$location.path('response-documents');
				});
			}
		};

		// Update existing Response document
		$scope.update = function() {
			var responseDocument = $scope.responseDocument;

			responseDocument.$update(function() {
				$location.path('response-documents/' + responseDocument._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Response documents
		$scope.find = function() {
			$scope.responseDocuments = ResponseDocuments.query();
		};

		// Find existing Response document
		$scope.findOne = function() {
			$scope.responseDocument = ResponseDocuments.get({ 
				responseDocumentId: $stateParams.responseDocumentId
			});
		};
	}
]);