'use strict';

// Configuring the Articles module
angular.module('providers').run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem('topbar', 'Поставщики', 'providers', 'dropdown', '/providers(/create)?', true, ['moderator'], 0);
		Menus.addSubMenuItem('topbar', 'providers', 'Список поставщиков', 'providers');
		Menus.addSubMenuItem('topbar', 'providers', 'Новый поставщик', 'providers/create');
	}
]);
