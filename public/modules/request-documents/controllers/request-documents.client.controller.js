'use strict';

// Request documents controller
angular.module('request-documents').controller('RequestDocumentsController', ['Docs', '$scope', '$stateParams', '$location', 'Authentication', 'RequestDocuments', 'Providers',
	function(Docs, $scope, $stateParams, $location, Authentication, RequestDocuments, Providers) {
		$scope.authentication = Authentication;
		function errorHandler(err){
			console.error(err);
		}
		$scope.find_request = function(){
			$scope.accept_requestDocument = RequestDocuments.accept_get({
				requestDocumentId: $stateParams.requestDocumentId
			});
		};
		// Find a list of Request documents
		$scope.find = function() {
			$scope.requestDocuments = RequestDocuments.query();
		};
		$scope.findResponse = function(){
			$scope.requestDocuments = RequestDocuments.responded();
		};
		$scope.get_response = function(array, item){
			return array.filter(function(i){
				return i.doc_id === item._id;
			});
		};
		$scope.findAcceptRequest = function() {
			var requestDocumentId = $stateParams.requestDocumentId;
			$scope.acceptRequestDocuments = RequestDocuments.findAcceptRequest({
				requestDocumentId: $stateParams.requestDocumentId,
				email: $stateParams.email
			}, function(){
				console.log($scope.acceptRequestDocuments.docs);
				if($stateParams.d){
					var allowed = $stateParams.d;
					var parsed = angular.toJson(allowed);
					$scope.acceptRequestDocuments.docs = $scope.acceptRequestDocuments.docs.filter(function(doc){
						return parsed.indexOf(doc._id) > -1;
					});
					console.log($scope.acceptRequestDocuments.docs);
				}
				$scope.filled_docs = $scope.acceptRequestDocuments.responses.filter(function(res){
					return $scope.acceptRequestDocuments.docs.filter(function(doc){
						return ((doc._id === res.doc_id) && (res.provider_email === $stateParams.email));
					}).length > 0;
				});
				$scope.is_filled = function(doc){
					var filled = $scope.filled_docs.filter(function(_doc){
						return (_doc.doc_id === (doc._id || doc));
					});
					if(filled.length){
						return filled[0];
					} else {
						return false;
					}
				};
			}, angular.noop);
		};
		$scope.findProviders = function() {
			$scope.providers = Providers.query();
		};
		$scope.selected_providers = [];
		// Find existing Request document
		$scope.findOne = function() {
			$scope.requestDocument = RequestDocuments.get({ 
				requestDocumentId: $stateParams.requestDocumentId
			});
		};
		$scope.findOneAndPatch = function() {
			$scope.requestDocument = RequestDocuments.get({
				requestDocumentId: $stateParams.requestDocumentId
			});
		};
		$scope.accept_request_doc_submit = function(){
			if($scope.acceptRequestDocuments){
				$scope.acceptRequestDocuments.docs.forEach(function(doc){
					if(doc.dos_price){
						doc.doc_responses = doc.doc_responses || [];
						doc.doc_responses.push({
							response: {
								doc_id: doc._id,
								dos_price: doc.dos_price,
								price: doc.price,
								check: doc.check,
								comment: doc.comment
							},
							selected: false
						});
					}
				});
				$scope.acceptRequestDocuments.$send_answer({
					email: $stateParams.email
				}, function(){
					$scope.findAcceptRequest();
				});
			}
		};
		$scope.findRequestDocument = function() {
			if($scope.requestDocument.excel_id){
				$scope.requestDocument = RequestDocuments.findByExcelId({
					excelId: $scope.requestDocument.excel_id
				});
			}
		};
		$scope.create_update_request_document = function(){
			delete $scope.requestDocument.docs;
			$scope.requestDocument.$update();
		};
		$scope.create_document = function() {
			$scope.requestDocument.new_document = {
				name: this.name,
				measure: this.measure,
				amount: this.amount,
				notes: this.notes
			};
			$scope.requestDocument.$add_doc(function(successResponse){
				$scope.name = '';
				$scope.measure = '';
				$scope.amount = 0;
				$scope.notes = '';
			}, errorHandler);
		};
		$scope.remove_doc = function(doc){
			var doc_id = doc._id || doc;
			$scope.requestDocument.$remove_doc({docId: doc_id});
		};
		$scope.send = function(){
			if(!$scope.selected_providers.length){
				alert('no provider');
				return;
			}
			var selected_provider_ids = $scope.selected_providers.map(function(item){
				return item._id;
			});
			var selected_docs_ids = $scope.requestDocument.docs.filter(function(item){
				return item.selected === true;
			}).map(function(item){
				return item._id;
			});
			$scope.requestDocument.selected_providers = selected_provider_ids;
			$scope.requestDocument.selected_docs = selected_docs_ids;
			$scope.requestDocument.$send_email();
		};
		$scope.save_responses = function(){
			$scope.requestDocument.$update();
		};
		$scope.lock = function(){
			$scope.requestDocument.$lock();
		};
		$scope.qwe = function(e){
			console.log(e);
		};
	}
]);
