// by bwin on 11/18/15.

'use strict';

// Request documents controller
angular.module('request-documents').controller('RequestDocumentsManagerController', ['Docs', '$scope', '$stateParams', '$location', 'Authentication', 'RequestDocuments', 'Materials', '$http',
    function(Docs, $scope, $stateParams, $location, Authentication, RequestDocuments, Materials , $http) {
        $scope.authentication = Authentication;
        function errorHandler(err){
            console.error(err);
        }
        $scope.findMaterials = function(search){
            return $http({
                method: 'GET',
                url: 'materials',
                params:  {
                    search: search
                }
            }).then(function(successResponse){
                return successResponse.data;
            });
            //return Materials.query({
            //    search: search
            //});
        };
        $scope.findMeasures = function(search){
            $scope.measures = Materials.measures();
            //return Materials.measures({
            //    search: search
            //});
        };
        $scope.findRequestDocument = function() {
            if($scope.requestDocument.excel_id){
                $scope.requestDocument = RequestDocuments.findByExcelId({
                    excelId: $scope.requestDocument.excel_id
                });
            }
        };
        $scope.findOne = function() {
            $scope.requestDocument = RequestDocuments.get({
                requestDocumentId: $stateParams.requestDocumentId
            });
        };
        $scope.save = function(callback){
            $scope.requestDocument.$update(function(){
                if(callback) callback();
            });
        };
        $scope.send_client = function(calback){
            $scope.requestDocument.$send_client(function(){
                if(calback) calback();
            });
        };
        $scope.first_selected_response = function(doc){
            return doc.responses_documents.filter(function(item){
                return item.selected === true;
            })[0];
        };
        $scope.findLockedRequestDocument = function() {
            $scope.lockedRequestDocuments = RequestDocuments.locked();
        };
        $scope.unlock = function(){
            $scope.requestDocument.$unlock();
        };
        $scope.create_update_request_document = function(callback){
            delete $scope.requestDocument.docs;
            $scope.requestDocument.$update_request_document();
            if(callback){
                callback();
            }
        };
        $scope.create_document = function() {
            $scope.requestDocument.new_document = {
                measure: this.measure,
                amount: this.amount,
                notes: this.notes
            };
            if(typeof $scope.new_material === 'string'){
                $scope.requestDocument.new_material = {
                    name: $scope.new_material
                };
            } else if(typeof $scope.new_material === 'object') {
                $scope.requestDocument.material = $scope.new_material._id || $scope.new_material.id;
            }
            $scope.requestDocument.$add_doc(function(successResponse){
                console.log(successResponse);
                $scope.new_material = null;
                $scope.measure = '';
                $scope.amount = 0;
                $scope.notes = '';
            }, errorHandler);
        };
        $scope.remove_doc = function(doc){
            var doc_id = doc._id || doc;
            $scope.requestDocument.$remove_doc({docId: doc_id});
        };
    }
]);
