// by bwin on 11/18/15.

'use strict';

// Request documents controller
angular.module('request-documents').controller('RequestDocumentsProviderController', ['Docs', '$scope', '$stateParams', '$location', 'Authentication', 'RequestDocuments', 'FileUploader',
    function(Docs, $scope, $stateParams, $location, Authentication, RequestDocuments, FileUploader) {
        $scope.authentication = Authentication;
        function errorHandler(err){
            console.error(err);
        }
        $scope.get_response = function(array, item){
            return array.filter(function(i){
                return i.doc_id === item._id;
            });
        };
        $scope.findAcceptRequest = function() {
            var requestDocumentId = $stateParams.requestDocumentId;
            $scope.acceptRequestDocuments = RequestDocuments.findAcceptRequest({
                requestDocumentId: $stateParams.requestDocumentId,
                email: $stateParams.email
            }, function(successResponse){
                console.log(successResponse);
                if($stateParams.d){
                    var allowed = $stateParams.d;
                    var parsed = angular.toJson(allowed);
                    $scope.acceptRequestDocuments.docs = $scope.acceptRequestDocuments.docs.filter(function(doc){
                        return parsed.indexOf(doc._id) > -1;
                    });
                }
                $scope.acceptRequestDocuments.docs.forEach(function(doc){
                    var responded_docs_of_provider = doc.responses_documents.filter(function(response){
                        return response.provider_email === $stateParams.email;
                    });
                    if(responded_docs_of_provider.length){
                        doc.old_response = responded_docs_of_provider[0].answer;
                    }
                });
            }, angular.noop);
        };
        $scope.accept_request_doc_submit = function(){
            if($scope.acceptRequestDocuments){
                $scope.acceptRequestDocuments.$send_answer({
                    email: $stateParams.email
                }, function(){
                    $scope.findAcceptRequest();
                });
            }
        };
        $scope.create_update_request_document = function(){
            delete $scope.requestDocument.docs;
            $scope.requestDocument.$update();
        };
        $scope.lock = function(){
            $scope.requestDocument.$lock();
        };
        var uploader = $scope.uploader = new FileUploader({
            url: '/upload',
            autoUpload: true
        });
        uploader.filters.push({
            name: 'imageFilter',
            fn: function(item, options) {
                var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                return '|jpg|png|jpeg|bmp|gif|pdf|doc|xls|xlsx|docx'.indexOf(type) !== -1;
            }
        });
        uploader.onCompleteItem = function(fileItem, response, status, headers) {
            $scope.acceptRequestDocuments.file_src = response;
            $scope.acceptRequestDocuments.$add_file(function(){
                $scope.findAcceptRequest();
            });
        };
    }
]);
