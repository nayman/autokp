// by bwin on 11/18/15.

// by bwin on 11/18/15.

'use strict';

// Request documents controller
angular.module('request-documents').controller('RequestDocumentsModeratorController', ['Docs', '$scope', '$stateParams', '$location', 'Authentication', 'RequestDocuments', 'Providers',
    function(Docs, $scope, $stateParams, $location, Authentication, RequestDocuments, Providers) {
        $scope.authentication = Authentication;
        function errorHandler(err){
            console.error(err);
        }
        $scope.find_request = function(){
            $scope.accept_requestDocument = RequestDocuments.accept_get({
                requestDocumentId: $stateParams.requestDocumentId
            });
        };
        // Find a list of Request documents
        $scope.find = function() {
            $scope.requestDocuments = RequestDocuments.query();
        };
        $scope.findResponse = function(){
            $scope.requestDocuments = RequestDocuments.responded();
        };
        $scope.findLockedResponse = function(){
            $scope.requestDocuments = RequestDocuments.locked();
        };
        $scope.get_response = function(array, item){
            return array.filter(function(i){
                return i.doc_id === item._id;
            });
        };
        $scope.findProviders = function() {
            $scope.providers = Providers.query();
        };
        $scope.selected_providers = [];
        // Find existing Request document
        $scope.findOne = function() {
            $scope.requestDocument = RequestDocuments.get({
                requestDocumentId: $stateParams.requestDocumentId
            });
        };
        $scope.findOneAndPatch = function() {
            $scope.requestDocument = RequestDocuments.get({
                requestDocumentId: $stateParams.requestDocumentId
            }, function(){
                if($stateParams.m){
                    $scope.requestDocument.docs.forEach(function(doc){
                        var existance = doc.responses_documents.filter(function(req){
                           return req.self_registered === true;
                        });
                        if(!existance.length){
                            console.log('no self doc');
                            doc.responses_documents.push({
                                provider_email: $stateParams.m,
                                answer: {
                                    price: 0,
                                    price_deliver: 0,
                                    exist: false,
                                    notes: ''
                                },
                                self_registered: true,
                                selected: false
                            });
                        } else {
                            console.log('self doc');
                        }
                    });
                }
            });
        };
        $scope.set_radio = function(doc, response){
            doc.responses_documents.forEach(function(r){
                r.selected = false;
            });
            response.selected = true;
        };
        $scope.send = function(){
            if(!$scope.selected_providers.length){
                alert('no provider');
                return;
            }
            var selected_provider_ids = $scope.selected_providers.map(function(item){
                return item._id;
            });
            var selected_docs_ids = $scope.requestDocument.docs.filter(function(item){
                return item.selected === true;
            }).map(function(item){
                return item._id;
            });
            $scope.requestDocument.selected_providers = selected_provider_ids;
            $scope.requestDocument.selected_docs = selected_docs_ids;
            $scope.requestDocument.$send_email();
        };
        $scope.save_responses = function(callback){
            if($stateParams.m){
                $scope.requestDocument.responded = true;
            }
            $scope.requestDocument.$update(function(){
                if(callback) callback();
            });
        };
        $scope.lock = function(){
            $scope.requestDocument.$lock();
        };
        $scope.unlock_request_document = function(){
            $scope.requestDocument.$unlock();
        };
        $scope.qwe = function(e){
            console.log(e);
        };
        $scope.providerResponses = function(arr, attr){
            if(!attr){
                return arr.filter(function(item){
                    return item.self_registered === attr;
                });
            } else {
                var item = arr.filter(function(item){
                    return item.self_registered === attr;
                })[0];
                return item;
            }
        }
    }
]);
