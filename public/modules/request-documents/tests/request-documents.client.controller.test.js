'use strict';

(function() {
	// Request documents Controller Spec
	describe('Request documents Controller Tests', function() {
		// Initialize global variables
		var RequestDocumentsController,
		scope,
		$httpBackend,
		$stateParams,
		$location;

		// The $resource service augments the response object with methods for updating and deleting the resource.
		// If we were to use the standard toEqual matcher, our tests would fail because the test values would not match
		// the responses exactly. To solve the problem, we define a new toEqualData Jasmine matcher.
		// When the toEqualData matcher compares two objects, it takes only object properties into
		// account and ignores methods.
		beforeEach(function() {
			jasmine.addMatchers({
				toEqualData: function(util, customEqualityTesters) {
					return {
						compare: function(actual, expected) {
							return {
								pass: angular.equals(actual, expected)
							};
						}
					};
				}
			});
		});

		// Then we can start by loading the main application module
		beforeEach(module(ApplicationConfiguration.applicationModuleName));

		// The injector ignores leading and trailing underscores here (i.e. _$httpBackend_).
		// This allows us to inject a service but then attach it to a variable
		// with the same name as the service.
		beforeEach(inject(function($controller, $rootScope, _$location_, _$stateParams_, _$httpBackend_) {
			// Set a new global scope
			scope = $rootScope.$new();

			// Point global variables to injected services
			$stateParams = _$stateParams_;
			$httpBackend = _$httpBackend_;
			$location = _$location_;

			// Initialize the Request documents controller.
			RequestDocumentsController = $controller('RequestDocumentsController', {
				$scope: scope
			});
		}));

		it('$scope.find() should create an array with at least one Request document object fetched from XHR', inject(function(RequestDocuments) {
			// Create sample Request document using the Request documents service
			var sampleRequestDocument = new RequestDocuments({
				name: 'New Request document'
			});

			// Create a sample Request documents array that includes the new Request document
			var sampleRequestDocuments = [sampleRequestDocument];

			// Set GET response
			$httpBackend.expectGET('request-documents').respond(sampleRequestDocuments);

			// Run controller functionality
			scope.find();
			$httpBackend.flush();

			// Test scope value
			expect(scope.requestDocuments).toEqualData(sampleRequestDocuments);
		}));

		it('$scope.findOne() should create an array with one Request document object fetched from XHR using a requestDocumentId URL parameter', inject(function(RequestDocuments) {
			// Define a sample Request document object
			var sampleRequestDocument = new RequestDocuments({
				name: 'New Request document'
			});

			// Set the URL parameter
			$stateParams.requestDocumentId = '525a8422f6d0f87f0e407a33';

			// Set GET response
			$httpBackend.expectGET(/request-documents\/([0-9a-fA-F]{24})$/).respond(sampleRequestDocument);

			// Run controller functionality
			scope.findOne();
			$httpBackend.flush();

			// Test scope value
			expect(scope.requestDocument).toEqualData(sampleRequestDocument);
		}));

		it('$scope.create() with valid form data should send a POST request with the form input values and then locate to new object URL', inject(function(RequestDocuments) {
			// Create a sample Request document object
			var sampleRequestDocumentPostData = new RequestDocuments({
				name: 'New Request document'
			});

			// Create a sample Request document response
			var sampleRequestDocumentResponse = new RequestDocuments({
				_id: '525cf20451979dea2c000001',
				name: 'New Request document'
			});

			// Fixture mock form input values
			scope.name = 'New Request document';

			// Set POST response
			$httpBackend.expectPOST('request-documents', sampleRequestDocumentPostData).respond(sampleRequestDocumentResponse);

			// Run controller functionality
			scope.create();
			$httpBackend.flush();

			// Test form inputs are reset
			expect(scope.name).toEqual('');

			// Test URL redirection after the Request document was created
			expect($location.path()).toBe('/request-documents/' + sampleRequestDocumentResponse._id);
		}));

		it('$scope.update() should update a valid Request document', inject(function(RequestDocuments) {
			// Define a sample Request document put data
			var sampleRequestDocumentPutData = new RequestDocuments({
				_id: '525cf20451979dea2c000001',
				name: 'New Request document'
			});

			// Mock Request document in scope
			scope.requestDocument = sampleRequestDocumentPutData;

			// Set PUT response
			$httpBackend.expectPUT(/request-documents\/([0-9a-fA-F]{24})$/).respond();

			// Run controller functionality
			scope.update();
			$httpBackend.flush();

			// Test URL location to new object
			expect($location.path()).toBe('/request-documents/' + sampleRequestDocumentPutData._id);
		}));

		it('$scope.remove() should send a DELETE request with a valid requestDocumentId and remove the Request document from the scope', inject(function(RequestDocuments) {
			// Create new Request document object
			var sampleRequestDocument = new RequestDocuments({
				_id: '525a8422f6d0f87f0e407a33'
			});

			// Create new Request documents array and include the Request document
			scope.requestDocuments = [sampleRequestDocument];

			// Set expected DELETE response
			$httpBackend.expectDELETE(/request-documents\/([0-9a-fA-F]{24})$/).respond(204);

			// Run controller functionality
			scope.remove(sampleRequestDocument);
			$httpBackend.flush();

			// Test array after successful delete
			expect(scope.requestDocuments.length).toBe(0);
		}));
	});
}());