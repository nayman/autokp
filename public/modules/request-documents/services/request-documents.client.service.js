'use strict';

//Request documents service used to communicate Request documents REST endpoints
angular.module('request-documents').factory('RequestDocuments', ['$resource',
	function($resource) {
		return $resource('request-documents/:requestDocumentId', { requestDocumentId: '@_id', excelId: '@excel_id'
		}, {
			update: {
				method: 'PUT'
			},
			send_email: {
				method: 'POST',
				url: 'request-documents/:requestDocumentId/send_email'
			},
			findAcceptRequest: {
				method: 'GET',
				url: 'request-documents/accept/:requestDocumentId',
				isArray: false
			},
			send_answer: {
				method: 'POST',
				url: 'request-documents/accept/:requestDocumentId/answer',
				isArray: false
			},
			findByExcelId: {
				method: 'GET',
				url: 'request-documents_excel/:excelId',
				isArray: false
			},
			add_doc: {
				method: 'POST',
				url: 'request-documents/:requestDocumentId/doc',
				isArray: false
			},
			remove_doc: {
				method: 'DELETE',
				url: 'request-documents/:requestDocumentId/doc',
				isArray: false
			},
			responded: {
				method: 'GET',
				url: 'responded-documents',
				isArray: true
			},
			lock: {
				method: 'POST',
				url: 'request-documents/:requestDocumentId/lock',
				isArray: false
			},
			unlock: {
				method: 'POST',
				url: 'request-documents/:requestDocumentId/unlock',
				isArray: false
			},
			locked: {
				method: 'GET',
				url: 'request-documents_locked',
				isArray: true
			},
			send_client: {
				method: 'POST',
				url: 'request-documents/:requestDocumentId/send_client',
				isArray: false
			},
			update_request_document: {
				method: 'PUT',
				url: '/request-documents-misc/:requestDocumentId/update_request_document',
				isArray: false
			},
			add_file: {
				method: 'PUT',
				url: '/request-documents-misc/:requestDocumentId/add_file',
				isArray: false
			}
		});
	}
]);
