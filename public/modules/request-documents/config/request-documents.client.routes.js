'use strict';

//Setting up route
angular.module('request-documents').config(['$stateProvider',
	function($stateProvider) {
		// Request documents state routing
		$stateProvider.
		state('newRequestDocument', {
			url: '/new-request-document',
			templateUrl: 'modules/request-documents/views/new-request-document.client.view.html'
		}).
		state('listRequestDocuments', {
			url: '/request-documents',
			templateUrl: 'modules/request-documents/views/list-request-documents.client.view.html'
		}).
		state('listResponseDocuments', {
			url: '/response-documents',
			templateUrl: 'modules/request-documents/views/list-response-documents.client.view.html'
		}).
		state('createRequestDocument', {
			url: '/request-documents/create',
			templateUrl: 'modules/request-documents/views/create-request-document.client.view.html'
		}).
		state('acceptRequestDocument', {
			url: '/request-documents/accept/:requestDocumentId?email&d',
			templateUrl: 'modules/request-documents/views/accept-request-document.client.view.html'
		}).
		state('viewRequestDocument', {
			url: '/request-documents/:requestDocumentId',
			templateUrl: 'modules/request-documents/views/view-request-document.client.view.html'
		}).
		state('listLockedRequestDocument', {
			url: '/response-documents-locked',
			templateUrl: 'modules/request-documents/views/list-locked-response-documents.client.view.html'
		}).
			state('listLockedRequestDocumentModerator', {
				url: '/response-documents-locked-moderator',
				templateUrl: 'modules/request-documents/views/list-locked-response-documents-moderator.client.view.html'
			}).
		state('viewLockedRequestDocument', {
			url: '/response-documents-locked/:requestDocumentId',
			templateUrl: 'modules/request-documents/views/view-locked-response-document.client.view.html'
		}).
			state('viewLockedRequestDocumentModerator', {
				url: '/response-documents-locked-moderator/:requestDocumentId',
				templateUrl: 'modules/request-documents/views/view-locked-response-document-moderator.client.view.html'
			}).
		state('viewResponseDocument', {
			url: '/response-documents/:requestDocumentId?m',
			templateUrl: 'modules/request-documents/views/view-response-document.client.view.html'
		});
	}
]);
