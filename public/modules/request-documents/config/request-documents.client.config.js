'use strict';

// Configuring the Articles module
angular.module('request-documents').run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem('topbar', 'Документы', 'request-documents', 'dropdown', '/request-documents(/create)?', true, ['moderator'], 0);
		Menus.addSubMenuItem('topbar', 'request-documents', 'Запросы от менеджера', 'request-documents');
		Menus.addSubMenuItem('topbar', 'request-documents', 'Ответы поставщиков', 'response-documents');
		Menus.addMenuItem('topbar', 'Отправленные', 'response-documents-locked-moderator', 'link', '', true, ['moderator'], 0);
		Menus.addMenuItem('topbar', 'Новый документ', 'new-request-document', 'link', '', true, ['manager'], 0);
		Menus.addMenuItem('topbar', 'Ответы от аккаунт-менеджера', 'response-documents-locked', 'link', '', true, ['manager'], 0);
		//Menus.addSubMenuItem('topbar', 'request-documents', 'New Request document', 'request-documents/create');
	}
]);
