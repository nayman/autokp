'use strict';

// Docs controller
angular.module('docs').controller('DocsController', ['$scope', '$stateParams', '$location', 'Authentication', 'Docs', 'RequestDocuments',
	function($scope, $stateParams, $location, Authentication, Docs, RequestDocuments) {
		$scope.authentication = Authentication;
		$scope.requestDocument = {};
		// Create new Doc
		$scope.create = function() {
			// Create new Doc object
			var doc = new Docs ({
				name: this.name,
				measure: this.measure,
				amount: this.amount,
				notes: this.notes
			});

			// Redirect after save
			doc.$save(function(response) {
				// Clear form fields
				$scope.name = '';
				$scope.measure = '';
				$scope.amount = 0;
				$scope.notes = '';
				$scope.find();
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Remove existing Doc
		$scope.remove = function(doc) {
			if (doc) {
				doc.$remove();
				for (var i in $scope.docs) {
					if ($scope.docs [i] === doc) {
						$scope.docs.splice(i, 1);
					}
				}
			} else {
				$scope.doc.$remove(function() {
					$location.path('docs');
				});
			}
		};

		// Find a list of Docs
		$scope.find = function() {
			$scope.docs = Docs.query({
				excel_id: $scope.excel_id
			});
		};

		// Find existing Doc
		$scope.findRequestDocument = function() {
			$scope.requestDocument = RequestDocuments.findByExcelId({
				excel_id: $scope.requestDocument.excel_id
			});
			console.log($scope.requestDocument);
		};
		$scope.create_update_request_document = function(){
			var requestDocument = new RequestDocuments({
				excel_id: $scope.excel_id,
				name: $scope.excel_name,
				email: $scope.excel_email,
				phone: $scope.excel_phone,
				city: $scope.excel_city,
				docs: $scope.docs
			});
			requestDocument.$save(function(){

			});
		};
	}
]);
